# 3rd party or built-in libraries
import numpy as np


class MathUtils:
    @staticmethod
    def normalized(data_to_normalize: np.ndarray, min_value: int, max_value: int) -> np.ndarray:
        """
        Basic data normalization.\n
        Formula: (`data_to_normalize` - `min_value`) / (`max_value` - `min_value`)

        Args:
            data_to_normalize: Data to be normalized
            min_value: Minimum value
            max_value: Maximum value

        Returns:
            Normalized data in range (min_value, max_value)
        """
        return (data_to_normalize - min_value) / (max_value - min_value)

    @staticmethod
    def subsequence_arg_range(samples: np.ndarray, max_length: int) -> list:
        """
        Function find range of subsequence of data where separator is max length between samples.

        Args:
            samples: One dimension array of numbers data (samples)
            max_length: Maximum length between samples (separator)

        Returns:
            Subspaces indices
        """
        diff = np.diff(samples)

        arg_bottom = 0
        arg_top = np.argmax(diff > max_length)

        range_value = list()
        while arg_top:
            range_value.append([arg_bottom, arg_bottom + arg_top])
            arg_bottom += arg_top + 1
            arg_top = np.argmax(diff[arg_bottom + 1:] > max_length)

        range_value.append([arg_bottom, samples.shape[0]])
        return range_value
