# 3rd party or built-in libraries
import numpy as np
import quaternion as qt
import matplotlib.pyplot as plt

import drone_stabilization_parameter


class DroneStabilizationVisualization:
    """
    Class responsible for all visualizations related to drone stabilization
    """
    @staticmethod
    def display_quaternion_comparison(quaternion_one: np.ndarray, timestamp_one: np.ndarray,
                                      quaternion_two: np.ndarray, timestamp_two: np.ndarray, title: str = ''):
        """
        Function only for comparison two ndarray of the np.quaternion

        Args:
            quaternion_one (np.ndarray): First quaternion
            timestamp_one (np.ndarray): Timestamp for first quaternion
            quaternion_two (np.ndarray): Second quaternion
            timestamp_two (np.ndarray): Timestamp for second quaternion
            title (str, optional): Title of the graphs. Default is empty.
        """
        titles = ["quaternion.w", "quaternion.x", "quaternion.y", "quaternion.z", ]
        quaternion_one_float_array = qt.as_float_array(quaternion_one)
        quaternion_two_float_array = qt.as_float_array(quaternion_two)

        plt.figure(figsize=(10, 9))
        plt.tight_layout(pad=5)

        for i in range(drone_stabilization_parameter.QUATERNION_LENGTH):
            plt.subplot(2, 2, i + 1)
            plt.plot(timestamp_two, quaternion_two_float_array[:, i], label='Est')
            plt.plot(timestamp_one, quaternion_one_float_array[:, i], label='Real')
            plt.ylabel("Value")
            plt.xlabel("Timestamp")
            plt.title(titles[i])
            plt.legend(loc='upper right')

        plt.suptitle(title)
        plt.show()
