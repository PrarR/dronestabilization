# 3rd party or built-in libraries
from quaternion.calculus import spline_evaluation
from quaternion import squad
import numpy as np
import quaternion as qt

# project classes
from math_utils import MathUtils


class QuaternionUtils:
    """
    Static class containing utils for quaternions: linear transformations, interpolation, autocorrelation,
    multiplication, transformation of Euler angles to rotation in Quaternion and more.
    """

    @staticmethod
    def spline_interpolation(q_arr: np.ndarray, t_arr: np.ndarray, t_interp_arr: np.ndarray,
                             spline_degree: int = 1):
        """
        Interpolation quaternion by spline method

        Args:
            q_arr (np.ndarray): Quaternion data array
            t_arr (np.array): Timestamp of `q_arr`
            t_interp_arr (np.ndarray): Timestamp of interpolated data, must be include in `t_arr` range value
            spline_degree (int, optional): Degree of spline method in range `1...5`. Defaults to 1.

        Returns:
            q_interp_arr (np.ndarray): Interpolated quaternions
        """
        if t_arr[0] > t_interp_arr[0]:
            print("Interpolated timestamp is smaller than the range of the data array")
            return

        if t_arr[-1] < t_interp_arr[-1]:
            print("Interpolated timestamp is bigger than the range of the data array")
            return

        # Normalization
        min_val = t_arr[0]
        max_val = t_arr[-1]

        t_arr_norm = MathUtils.normalized(t_arr, min_val, max_val)
        t_interp_arr_norm = MathUtils.normalized(t_interp_arr, min_val, max_val)

        # Unpack quaternion
        q_arr_unpacked = qt.as_float_array(q_arr)

        # Interpolation
        q_interp_unpacked = spline_evaluation(q_arr_unpacked, t_arr_norm, t_interp_arr_norm,
                                              spline_degree=spline_degree)

        # Pack quaternion to np.quaternion and return
        return qt.from_float_array(q_interp_unpacked)

    @staticmethod
    def squad_interpolation(q_arr: np.ndarray, t_arr: np.ndarray, t_interp_arr: np.ndarray):
        """
        Interpolation quaternion by squad method, based on Slerp method

        Args:
            q_arr: Quaternion data array
            t_arr: Timestamp of `q_arr`
            t_interp_arr: Timestamp of interpolated data, must be include in `t_arr` range value

        Returns:
            Interpolated np.ndarray of the np.quaternion
        """
        if t_arr[0] > t_interp_arr[0]:
            print("Interpolated timestamp is smaller than the range of the data array")
            return

        if t_arr[-1] < t_interp_arr[-1]:
            print("Interpolated timestamp is bigger than the range of the data array")
            return

        # Normalization
        min_val = t_arr[0]
        max_val = t_arr[-1]

        t_arr_norm = MathUtils.normalized(t_arr, min_val, max_val)
        t_interp_arr_norm = MathUtils.normalized(t_interp_arr, min_val, max_val)

        # Interpolation
        q_interp = squad(q_arr, t_arr_norm, t_interp_arr_norm)

        return q_interp

    @staticmethod
    def parse_str_ndarray_to_quaternions_ndarray(data: np.ndarray, keys: list[str]) -> np.ndarray:
        """
        Parse str ndarray to array of the np.quaternion.

        Args:
            data: np.ndarray containing quaternions.
            keys: four keys used to read the value for each quaternion component: qw, qx, qy, qw.

        Returns:
            np.ndarray of the np.quaternion
        """
        if len(keys) != 4:
            print("Quaternions need four keys for qw, qx, qy, qz!")
            return np.empty(dtype=np.quaternion, shape=(0, 0))

        data_size = np.shape(data)[0]
        data = zip(data[keys[0]], data[keys[1]], data[keys[2]], data[keys[3]])
        quaternions = np.zeros(data_size, dtype=np.quaternion)
        for i, m in enumerate(data):
            quaternions[i] = np.quaternion(m[0], m[1], m[2], m[3])
        return quaternions

    @staticmethod
    def quaternion_interpolation(quat_one: np.quaternion, quat_two: np.quaternion, t: float) -> np.quaternion:
        """
        Quaternion spherical linear interpolation.

        Args:
            quat_one: First quaternion.
            quat_two: Second quaternion.
            t: Normalized range from quat_one to quat_two.

        Returns:
            interpolated quaternion.
        """
        return (quat_two * quat_one.inverse()) ** t * quat_one

    @staticmethod
    def quaternion_autocorrelation(input_array: np.ndarray) -> np.ndarray:
        """
        Calculate autocorrelation of given quaternions.

        Args:
            input_array: input np.ndarray of the np.quaternion type.

        Returns:
            np.ndarray of the np.quaternion
        """
        quaternions_size = input_array.shape[0]
        quaternions = np.zeros(quaternions_size, dtype=np.quaternion)

        for m in range(quaternions_size):
            mth_quat = input_array[m]
            for q in input_array:
                quaternions[m] += QuaternionUtils.quaternion_interpolation(q, mth_quat, 0.5)

        return quaternions

    @staticmethod
    def get_quaternion_ndarray_from_euler_angles_ndarray(euler_angles: np.ndarray, keys: list[str]) -> np.ndarray:
        """
        Return np.ndarray of the np.quaternion based on the given euler angles array.

        Args:
            euler_angles: input x, y, z rotation
            keys: three keys used to read the value for each euler angles component: x, y, z.

        Returns:
            np.ndarray of the np.quaternion
        """
        if len(keys) != 3:
            print("Euler angles need three keys for x, y, z rotation!")
            return np.empty(dtype=np.quaternion, shape=(0, 0))

        return qt.from_euler_angles(euler_angles[keys[0]], euler_angles[keys[1]], euler_angles[keys[2]])

    @staticmethod
    def get_quaternion_from_euler_angles(roll: float, pitch: float, yaw: float) -> list[float]:
        """
        Return np.quaternion based on the given euler angles.

        Args:
            roll: The roll (rotation around x-axis) angle in radians.
            pitch: The pitch (rotation around y-axis) angle in radians.
            yaw: The yaw (rotation around z-axis) angle in radians.

        Returns:
            The orientation in quaternion [w, x, y, z] format.
        """
        half_roll = roll / 2
        half_pitch = pitch / 2
        half_yaw = yaw / 2
        qw = np.cos(half_roll) * np.cos(half_pitch) * np.cos(half_yaw) + np.sin(half_roll) * np.sin(
            half_pitch) * np.sin(half_yaw)
        qx = np.sin(half_roll) * np.cos(half_pitch) * np.cos(half_yaw) - np.cos(half_roll) * np.sin(
            half_pitch) * np.sin(half_yaw)
        qy = np.cos(half_roll) * np.sin(half_pitch) * np.cos(half_yaw) + np.sin(half_roll) * np.cos(
            half_pitch) * np.sin(half_yaw)
        qz = np.cos(half_roll) * np.cos(half_pitch) * np.sin(half_yaw) - np.sin(half_roll) * np.sin(
            half_pitch) * np.cos(half_yaw)
        return [qw, qx, qy, qz]

    @staticmethod
    def quaternion_multiply(quat_one: list[float], quat_two: list[float]) -> list[float]:
        """
        Multiplication of one quaternion by another. Note: Multiplication order matters!

        Args:
            quat_one: A 4 element data containing the first quaternion.
            quat_two: A 4 element data containing the second quaternion.

        Returns:
            A 4 element data containing the final quaternion (w, x, y, z)
        """
        w0, x0, y0, z0 = quat_one[0], quat_one[1], quat_one[2], quat_one[3]
        w1, x1, y1, z1 = quat_two[0], quat_two[1], quat_two[2], quat_two[3]

        q0q1_w = w0 * w1 - x0 * x1 - y0 * y1 - z0 * z1
        q0q1_x = w0 * x1 + x0 * w1 + y0 * z1 - z0 * y1
        q0q1_y = w0 * y1 - x0 * z1 + y0 * w1 + z0 * x1
        q0q1_z = w0 * z1 + x0 * y1 - y0 * x1 + z0 * w1

        return [q0q1_w, q0q1_x, q0q1_y, q0q1_z]

    @staticmethod
    def quaternions_dot(arr_1: np.ndarray, arr_2: np.ndarray):
        """
        Function that calculates the dot product of two quaternions.

        Args:
            arr_1: first array (quaternion)
            arr_2: second array

        Returns:
            Dot product of two quaternions.
        """
        dot = np.quaternion(0, 0, 0, 0)
        for q1, q2 in zip(arr_1, arr_2):
            dot += q1 * q2

        return dot
