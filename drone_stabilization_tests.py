# 3rd party or built-in libraries
from random import randint
import matplotlib.pyplot as plt
import numpy as np


# project classes
from circular_buffer import CircularBuffer
from quaternion_utils import QuaternionUtils


class DroneStabilizationTests:
    """
    A test class for the DroneStabilization class.
    """
    def __init__(self, vehicle_attitude_quaternions: np.quaternion, vehicle_attitude_timestamp: np.ndarray):
        """
        Constructor of the DroneStabilizationTests class.

        Args:
            vehicle_attitude_quaternions: quaternions from vehicle sensors
            vehicle_attitude_timestamp: timestamps from vehicle sensors
        """
        self.vehicle_attitude_quaternions = vehicle_attitude_quaternions
        self.vehicle_attitude_timestamp = vehicle_attitude_timestamp

    def tests(self):
        """
        For test purposes
        """
        quat = self.vehicle_attitude_quaternions
        t_stamp = self.vehicle_attitude_timestamp

        filter_order = 8

        offset = randint(0, 4000)
        r_mat = QuaternionUtils.quaternion_autocorrelation(quat[offset:offset + filter_order][::-1])

        a_arr = np.zeros(0, dtype=np.quaternion)
        r_arr = np.zeros(0, dtype=np.quaternion)

        r0 = 1

        a_arr = np.append(a_arr, r_mat[0])
        r_arr = np.append(r_arr, r_mat[0])

        for i in range(1, filter_order):
            temp_a = (r_mat[i] - QuaternionUtils.quaternions_dot(r_arr[::-1], a_arr)) * (
                    r0 - QuaternionUtils.quaternions_dot(r_arr[::-1], a_arr[::-1])).inverse()
            a_arr = np.append(a_arr, temp_a)
            for j in range(i):
                a_arr[j] = a_arr[j] - temp_a * a_arr[i - j - 1]

            r_arr = np.append(r_arr, r_mat[i])

        print("Filter coefficient: \n", a_arr)

        # Tests
        buffer = CircularBuffer(filter_order, np.quaternion)

        start_address = randint(0, 4000)

        for i in range(filter_order):  # Load buffer
            buffer.push(quat[start_address])
            start_address += 1

        for _ in range(10):
            print('EX: ', quat[start_address])
            print('Est: ', QuaternionUtils.quaternions_dot(a_arr, buffer.get_buffer()[:-1]))
            start_address += 1
            buffer.push(quat[start_address])

        t_stamp_diff = np.diff(t_stamp)
        t_stamp_diff_median = np.median(t_stamp_diff)

        print("Timestamp diff: {}".format(t_stamp_diff_median))
        counts, bins = np.histogram(t_stamp_diff[:np.argmax(t_stamp_diff)], bins=20)
        plt.stairs(counts, bins)
        plt.xlim(0, 2e4)
        plt.show()

        val = np.random.randint(0, np.argmax(t_stamp_diff))

        for i in range(val, val + 6, 2):
            print("Sample {}: t_stamp: {}, quat: {}".format(i, t_stamp[i], quat[i]))

        t = t_stamp_diff_median / (t_stamp[val + 2] - t_stamp[val])
        est = QuaternionUtils.quaternion_interpolation(quat[val], quat[val + 2], t)
        print("\n")
        print("EST : t_stamp: {}, quat: {}".format(int(t_stamp[val] + t_stamp_diff_median), est))
        print("REAL: t_stamp: {}, quat: {}".format(t_stamp[val + 1], quat[val + 1]))

        dif = est - quat[val + 1]
        print(dif)
        mse = np.sqrt(dif.w ** 2 + dif.x ** 2 + dif.y ** 2 + dif.z ** 2)
        print("MSE error: {}".format(mse))
