# 3rd party or built-in libraries
import numpy as np


class DataLoader:
    """
    Class responsible for load data from files.
    """

    @staticmethod
    def load_data(path: str) -> np.ndarray:
        """
        Load data (formats: .csv, .txt; delimiter=',') from specific path.

        Args:
            path: file localization in project

        Returns:
            Data loaded from .csv / .txt file
        """
        return np.genfromtxt(path, names=True, delimiter=',')
     