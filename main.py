# project classes
from data_loader import DataLoader
from drone_stabilization import DroneStabilization

if __name__ == '__main__':
    folder_path = "data/"
    data_loader = DataLoader()
    navigator_setpoint_file_path = folder_path + "37_bs78_out_GPS_alt2pos_navigator_setpoint_0.csv"
    navigator_setpoint_data = data_loader.load_data(navigator_setpoint_file_path)
    vehicle_attitude_file_path = folder_path + "37_bs78_out_GPS_alt2pos_vehicle_attitude_0.csv"
    vehicle_attitude_data = data_loader.load_data(vehicle_attitude_file_path)
    drone_stabilization = DroneStabilization(navigator_setpoint_data, vehicle_attitude_data)
    drone_stabilization.compare_interpolation_methods()
