# 3rd party or built-in libraries
import numpy as np


class CircularBuffer:
    """
    Class implement circular buffer.
    """

    def __init__(self, length: int, data_type: np.dtype):
        """
        Constructor of the CircularBuffer class.

        Args:
            length: buffer length (size).
            data_type: type of data stored in buffer.
        """
        self.length = length
        self.data_type = data_type
        self.buffer = np.zeros((length, 1), dtype=data_type)
        self.pointer = 0

    def push(self, element: np.dtype):
        """
        Push element on top of the FIFO list

        Args:
            element: Element to be added at the end of the FIFO queue
        """
        self.pointer = self.pointer % self.length
        self.buffer[self.pointer] = element
        self.pointer += 1

    def get_buffer(self) -> np.array:
        """
        Return buffer in the FIFO sequence

        Returns:
            buffer
        """
        return np.roll(self.buffer, self.length - self.pointer)[::-1]
