# 3rd party or built-in libraries
import numpy as np

# project classes
from drone_stabilization_visualization import DroneStabilizationVisualization
from drone_stabilization_tests import DroneStabilizationTests
from quaternion_utils import QuaternionUtils
from math_utils import MathUtils
import drone_stabilization_parameter


class DroneStabilization:
    """
    Class responsible for drone rotation stabilization (in quaternion notation).
    Class accepts an input data read from measuring sensors to calculate rotation correction.
    """

    def __init__(self, navigator_setpoint_data: np.ndarray, vehicle_attitude_data: np.ndarray):
        """
        Constructor of the DroneStabilization class.

        Args:
            navigator_setpoint_data: data read from the drone navigator
            vehicle_attitude_data: data read from the vehicle sensors
        """
        self.navigator_setpoint_quaternions = QuaternionUtils.get_quaternion_ndarray_from_euler_angles_ndarray(
            navigator_setpoint_data, ['sp_Roll_angle', 'sp_Pitch_angle', 'sp_Yaw_angle'])
        self.vehicle_attitude_quaternions = QuaternionUtils.parse_str_ndarray_to_quaternions_ndarray(
            vehicle_attitude_data,
            ['q0', 'q1', 'q2', 'q3'])
        self.navigator_setpoint_timestamp = navigator_setpoint_data['timestamp']
        self.vehicle_attitude_timestamp = vehicle_attitude_data['timestamp']
        self.drone_stabilization_visualization = DroneStabilizationVisualization()
        self.drone_stabilization_tests = DroneStabilizationTests(self.vehicle_attitude_quaternions,
                                                                 self.vehicle_attitude_timestamp)
        # self.drone_stabilization_tests.tests()  # for test purposes

    def compare_interpolation_methods(self):
        """
        Compare two interpolation methods - spline & squad. Generate visualizations.
        """
        # Find subsequences and split data
        arg_partial_signal = MathUtils.subsequence_arg_range(self.vehicle_attitude_timestamp, max_length=200000)

        att_args = arg_partial_signal[0]
        att_t_part = self.vehicle_attitude_timestamp[att_args[0]:att_args[1]]
        att_q_part = self.vehicle_attitude_quaternions[att_args[0]:att_args[1]]

        nav_args = [np.argmin(self.navigator_setpoint_timestamp < att_t_part[0]),
                    np.argmax(self.navigator_setpoint_timestamp > att_t_part[-1])]
        nav_t_part = self.navigator_setpoint_timestamp[nav_args[0]:nav_args[1]]
        nav_q_part = self.navigator_setpoint_quaternions[nav_args[0]:nav_args[1]]

        min_value = np.max([np.min(att_t_part), np.min(nav_t_part)])
        max_value = np.min([np.max(att_t_part), np.max(nav_t_part)])

        # Timestamp for interpolated quaternion
        dt = drone_stabilization_parameter.TIME_BETWEEN_SAMPLES_IN_MILLISECONDS
        est_t_part = np.arange(min_value, max_value + 1, dt)

        # Interpolation
        spline_degree = 1  # NOTE Best result for spline_degree = 1
        est_q_att = QuaternionUtils.spline_interpolation(att_q_part, att_t_part, est_t_part)
        est_q_nav = QuaternionUtils.spline_interpolation(nav_q_part, nav_t_part, est_t_part)

        DroneStabilizationVisualization.display_quaternion_comparison(
            att_q_part, att_t_part, est_q_att, est_t_part, 'Spline Method Attitude, deg={}'.format(spline_degree))
        DroneStabilizationVisualization.display_quaternion_comparison(
            nav_q_part, nav_t_part, est_q_nav, est_t_part, 'Spline Method Navigator, deg={}'.format(spline_degree))
